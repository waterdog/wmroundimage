#
# Be sure to run `pod lib lint WMRoundImageView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "WMRoundImageView"
  s.version          = "0.4"
  s.summary          = "Image view with round image."
  s.swift_version    = "4.1"
  s.description      = <<-DESC
                        A subclass of UIImageView that presents a round image, with transparent background.

                        It's designable, which means it can be used on interface builder as an Apple's view.
                       DESC

  s.homepage         = "https://bitbucket.org/waterdog-cocoa/wmroundimage"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Jorge Galrito" => "jorge.galrito@me.com" }
  s.source           = { :git => "https://bitbucket.org/waterdog-cocoa/wmroundimage.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'UIKit'
end
