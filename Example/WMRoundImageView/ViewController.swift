//
//  ViewController.swift
//  WMRoundImageView
//
//  Created by Jorge Galrito on 09/16/2015.
//  Copyright (c) 2015 Jorge Galrito. All rights reserved.
//

import UIKit
import WMRoundImageView

class ViewController: UIViewController {

    @IBOutlet var interfaceBuilderImageView: RoundImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Create another programmatically
        let image = UIImage(named: "galaxy_universe-normal.jpg")
        let imageView = RoundImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(imageView)
        
        view.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:[ibImageView][imageView]|",
            options: [],
            metrics: nil,
            views: ["ibImageView": interfaceBuilderImageView, "imageView": imageView]
        ))
        view.addConstraint(NSLayoutConstraint(
            item: imageView,
            attribute: .height,
            relatedBy: .equal,
            toItem: imageView,
            attribute: .width,
            multiplier: 1.0,
            constant: 0.0)
        )
        view.addConstraint(NSLayoutConstraint(
            item: imageView,
            attribute: .top,
            relatedBy: .equal,
            toItem: interfaceBuilderImageView,
            attribute: .top,
            multiplier: 1.0,
            constant: 0.0)
        )
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

