//
//  RoundImageView.swift
//  Pods
//
//  Created by Jorge Galrito on 16/09/15.
//
//

import UIKit

@IBDesignable
public class RoundImageView: UIImageView {
    
    private let maskLayer: CAShapeLayer = CAShapeLayer()
    private let strokeLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        layer.fillColor = UIColor.clear.cgColor
        return layer
    }()
    
    @IBInspectable
    public var strokeColor: UIColor = UIColor.clear {
        didSet {
            strokeLayer.strokeColor = strokeColor.cgColor
        }
    }
    
    @IBInspectable
    public var strokeWidth: CGFloat = 0.0 {
        didSet {
            strokeLayer.lineWidth = strokeWidth
        }
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        configureImageView()
    }
    
    override public init(image: UIImage?) {
        super.init(image: image)
        configureImageView()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureImageView()
    }
    
    override public func layoutSubviews() {
        let path = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.width / 2).cgPath

        maskLayer.path = path
        maskLayer.frame = CGRect(
            x: 0, y: 0,
            width: bounds.width, height: bounds.height
        )
        
        strokeLayer.path = path
    }
    
    private func configureImageView() {
        contentMode = .scaleAspectFill
        maskLayer.fillColor = UIColor.black.cgColor
        layer.mask = maskLayer
        
        layer.addSublayer(strokeLayer)
    }
    
}
