# WMRoundImageView

[![CI Status](http://img.shields.io/travis/Jorge Galrito/WMRoundImageView.svg?style=flat)](https://travis-ci.org/Jorge Galrito/WMRoundImageView)
[![Version](https://img.shields.io/cocoapods/v/WMRoundImageView.svg?style=flat)](http://cocoapods.org/pods/WMRoundImageView)
[![License](https://img.shields.io/cocoapods/l/WMRoundImageView.svg?style=flat)](http://cocoapods.org/pods/WMRoundImageView)
[![Platform](https://img.shields.io/cocoapods/p/WMRoundImageView.svg?style=flat)](http://cocoapods.org/pods/WMRoundImageView)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

WMRoundImageView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "WMRoundImageView"
```

## Author

Jorge Galrito, jorge.galrito@me.com

## License

WMRoundImageView is available under the MIT license. See the LICENSE file for more info.
